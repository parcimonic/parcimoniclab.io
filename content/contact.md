+++
title = "Contact info"
description = "contact info page"
date = "2022-11-22"
lastmod = "2022-11-22"

[menu.main]
  identifier = "contact"
  name = "contact"
  title = "contact"
  url = "/contact/"
  weight = 2
+++

You can use the following resources to reach me:

- e-mail: contact@parcimonic.me

- mastodon: <a rel="me" href="https://fosstodon.org/@parcimonic">@parcimonic@fosstodon.org</a>

- matrix: @parcimonic:matrix.org

You can also open an issue in the blog's repository, currently hosted at GitLab:

https://gitlab.com/parcimonic/parcimonic.gitlab.io
