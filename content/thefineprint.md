+++
title = "The fine print"
description = "the more you know"
date = "2022-11-22"
lastmod = ""

[menu.main]
  identifier = "fineprint"
  name = "fineprint"
  title = "fineprint"
  url = "/fineprint/"
  weight = 3
+++

From time to time, I see cases of misunderstanding[^1] between the community and content producers, be it code or prose. Here, I'll state my take[^2] on FOSS content that I may work with.

[^1]: A comical [example](https://daniel.haxx.se/blog/2022/01/24/logj4-security-inquiry-response-required/) from the `curl` maintainer. Another [example](https://github.com/seebye/ueberzug/blob/b00c2685525f8014f464e7d648095a44550a37c1/README.md).

[^2]: I believe that *most* (myself included) open source software developers, project maintainers and documentation writers have good intentions and want to provide good service and content to the community.

Unless otherwise noted in the license or in a very clear position in the body of the text, the following applies:

### Code

- All code comes without any form of warranty or support.

- Pull or merge requests have no review or approval guarantee.

- Issues raised in repositories have no obligation of being acknowledged or resolved.

Note: the only thing between someone else's code and chaos in production is your judgment, so make good use of it.

### Text

All tutorials and similar "how-to" documentation may become outdated as soon as they're published. Always refer to the original or upstream documentation before applying described steps or running commands. If I didn't provide references in the text, feel free to [contact]({{< ref "/contact" >}}) me.
