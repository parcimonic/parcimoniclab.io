+++
title = "Hello World"
description = "first blog post"
date = "2022-11-22"
lastmod = ""
+++

The blog needs a first post, so here it is.

I'm using the [Hugo](https://gohugo.io/) framework and the [Etch](https://github.com/LukasJoswiak/etch) theme for the blog. I liked the easy setup and clean theme, with the benefit of [auto-darkmode](https://github.com/LukasJoswiak/etch/wiki/Dark-mode).

I've also set [syntax highlighting](https://github.com/LukasJoswiak/etch/wiki/Syntax-highlighting) for light and dark modes, so this should render appropriately for your choice of mode:

```yaml
some_key: some value
```

```rust
fn main () {
    println!("Hello World!");
}
```

The color theme for syntax highlighting is [monokai](https://xyproto.github.io/splash/docs/monokai.html), which I try to use everywhere.

That's all for today, folks.
